@extends('books.layout')

@section('content')

<h1 class="text-center">Libros</h1>

<div class="container">
	
<a class="btn btn-info mb-3" href="{{route('books.create')}}">Agregar libro</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($books as $book)
    <tr>
      <th scope="row">{{ $book->id }}</th>
      <td><a href="{{route('books.show',$book->id)}}">{{ $book->title }}</a></td>
      <td>{{ $book->description }}</td>
      <td><a class="btn btn-info" href="{{route('books.edit',$book->id)}}"><i class="fas fa-edit"></i></a>
      
      <!--Formulario de seguridad para eliminar-->
      <form action="{{route('books.destroy',$book->id)}}" method="POST">
      	@csrf
      	@method('DELETE')
      	<button type="submit" class="btn-sm btn-danger" onclick="return confirm('¿Quiere borrar el elemento?')"><i class="fas fa-trash-alt fa-2x"></i></button>
      </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>


{{$books->links()}}

</div>
@endsection