<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Session;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //traer todos los libros de la bd
        $books=Book::paginate(3);//permite hacer un menu en la parte de abajo de la pagina con index.blade.php en {{$books->links()}}

//order by permite organizar $books=Book::orderBy('id','DESC')->paginate(3);//
        return view('books.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //primero se valida si llega informacion
        $request->validate([
'title'=>'required',
'description'=>'required'
        ]);

        Book::create($request->all());

        Session::flash('message','Libro creado');//No realiza el mensaje
        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
        return view('books.show',compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
        return view('books.edit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
         $request->validate([
'title'=>'required',
'description'=>'required'
        ]);

$book->update($request->all());

Session::flash('message','Libro actualizado');//No realiza el mensaje
        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
        $book->delete();
        Session::flash('message','Libro eliminado correctamente');
       return redirect()->route('books.index');
    }
}